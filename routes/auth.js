'use strict';

const express = require('express');
const axios = require('axios').default;
const querystring = require('querystring'); // to handle x-www-form-urlencoded
const router = express.Router();

const { CLIENT_ID, CLIENT_SECRET, REDIRECT_URI } = process.env;

router.get('/', (req, res, next)  => {
  const { code, guild_id, permissions } = req.query;

  if (!code) {
    return res.render('index', { title: 'Toj-Bot' });
  }

  axios
    .post('https://discord.com/api/oauth2/token', querystring.stringify({
      client_id: CLIENT_ID,
      client_secret: CLIENT_SECRET,
      grant_type: 'authorization_code',
      code,
      redirect_uri: REDIRECT_URI
    }))
    .then(response => {

      // Object.keys(response)
      // [ 'status', 'statusText', 'headers', 'config', 'request', 'data' ]

      const {
        access_token,
        expires_in,
        refresh_token,
        scope,
        token_type
      } = response.data;

      console.log(JSON.stringify(response.data, null, 2));
      // TODO safe the data and stuff

      res.json(response.data);
    })
    .catch(next)

});

module.exports = router;
