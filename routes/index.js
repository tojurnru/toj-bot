'use strict';

const express = require('express');
const router = express.Router();

const usersRouter = require('./users');
const authRouter = require('./auth');

// Debug
router.use((req, res, next) => {
  console.log('=================================================================');
  console.log(`URL: ${req.url}`);
  console.log(`HEADERS: ${JSON.stringify(req.headers, null, 2)}`);
  console.log(`QUERY: ${JSON.stringify(req.query, null, 2)}`);
  console.log(`COOKIES: ${JSON.stringify(req.cookies, null, 2)}`);
  console.log(`PARAMS: ${JSON.stringify(req.params, null, 2)}`);
  console.log(`BODY: ${JSON.stringify(req.body, null, 2)}`);
  console.log('=================================================================');

  // Allow Access From Everywhere
  res.setHeader('Access-Control-Allow-Credentials', 'true');
  res.setHeader('Access-Control-Allow-Origin', '*');

  next();
});


// Simulate Internal Server Error
router.use('/server-error', (req, res, next) => next({ status: 500, message: 'Internal Server Error'}));


router.use('/users', usersRouter);


// Default Response
// router.get('/', (req, res, next) => res.render('index', { title: 'Toj-Bot' }));
router.use('/', authRouter)

module.exports = router;
